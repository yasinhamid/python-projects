import os
import cv2
import csv

MOVIES_ROOT_DIR = "v:/Filmy/"

print("Welcome")


def validfilename(filenamein):
    if filenamein.endswith('.avi') or filenamein.endswith('.mp4'):
        return True
    return False


header = ['filename','path', 'width', 'height', 'bitrate', 'fps']

with open('./fileList.csv', mode="w", encoding='utf8', newline='') as outputFile:
    wtr = csv.writer(outputFile)
    wtr.writerow(header)

    for path, dirs, files in os.walk(MOVIES_ROOT_DIR):
        #for dirname in dirs:
        #    print(os.path.join(path, dirname))
        for filename in files:
            #metadata = FFProbe("v:/Filmy/Snowpiercer (2013)/Snowpiercer (2013).avi")
            if (validfilename(filename) == False):
                continue
            file_path = os.path.join(path, filename)  # change to your own video path
            vid = cv2.VideoCapture(file_path)
            height = int(vid.get(cv2.CAP_PROP_FRAME_HEIGHT))
            width = int(vid.get(cv2.CAP_PROP_FRAME_WIDTH))
            bitrate = vid.get(cv2.CAP_PROP_BITRATE)
            fps = vid.get(cv2.CAP_PROP_FPS)
            r = [filename, file_path, width, height, bitrate, fps]
            wtr.writerow(r)
            #probe = ffprobe.probe(filename)
            #video_stream = next((stream for stream in probe['streams'] if stream['codec_type'] == 'video'), None)
            #if video_stream is None:
            #    print('No video stream found', file=sys.stderr)
            #    continue
            #width = int(video_stream['width'])
            #height = int(video_stream['height'])
            print(os.path.join(path, filename), ' ', height, 'x',width)


