# This script reads all csv files in fiven directory
# retrieving instagram account and follower account
# The result is wrriten into a text file with \t separator
# That way it can be process by map reduce

import argparse
from pathlib import Path
from csv import DictReader

# Construct an argument parser
all_args = argparse.ArgumentParser()

# Add arguments to the parser
all_args.add_argument("-in", "--inputDir", required=True,
   help="input directory")
all_args.add_argument("-out", "--outputFile", required=True,
   help="output file")
args = vars(all_args.parse_args())

inputDir = args['inputDir']
outputFile = args['outputFile']


print("Input directory is: {}".format(inputDir))
print("Output file is: {}".format(outputFile))

# Open function to open the file "MyFile1.txt"
# (same directory) in read mode and
outFile = open(outputFile, "w")

for p in Path(inputDir).glob('*.csv'):
    print(f"{p.name}")
    with open(p.absolute(), 'r', encoding="utf-8") as csvfile:
        # passing file object to DictReader()
        csv_dict_reader = DictReader(csvfile)
        # iterating over each row
        for rowData in csv_dict_reader:
            # print the values
            outFile.write(rowData['originalUser'] + "\t" + rowData['username'] + "\n")


outFile.close()
