from mrjob.job import MRJob
from mrjob.step import MRStep

# This reads input file in the format <instagram account ID>\t<account ID of a follower>
# Then it calculates sets of all combinations of <instagram account ID>
#
# E.g.
# @LEGO account is followed by @MyDad and @JohnyBoy and @CookieMonster
# @SesameStreet is followed by @JohnyBoy and @CookieMonster and @Kermit
# @Lingerie is followed by @MyDad
# Output will be:
# Lingerie 1
# SesameStreet 3
# LEGO 3
# LEGO,SesameStreet 2 (because they have two common followers - @JohnyBoy and @CookieMonster)
# LEGO,Lingerie 1 (only one common follower - @MyDad)
# Lingerie,SesameStreet (will not be present because there is no common follower)
# LEGO,Lingerie,SesameStreet (will not be present because there is no common follower)

class CountCommonFollowers(MRJob):
    def steps(self):
        return [
            MRStep(mapper=self.mapper_get_followers,
                   reducer=self.reducer_join_acc_names),
            #MRStep(mapper=self.mapper_passthrough,
            #       reducer = self.reducer_count_acc)
        ]

    # Retrieve insta account (userID) and account id of who is following it (followedByID)
    def mapper_get_followers(self, _, line):
        (userID, followedByID) = line.split('\t')
        yield followedByID, userID

    def reducer_join_acc_names(self, followedByID, userID):
        # This is a list of insta accounts being followed
        #Create a set (one occurence of a userID)
        users = set(userID)
        #Craete a joint key
        newKey = ','.join(users)
        yield newKey, followedByID

    # This mapper does nothing; it's just here to avoid a bug in some
    # versions of mrjob related to "non-script steps." Normally this
    # wouldn't be needed.
    def mapper_passthrough(self, key, value):
        yield key, value

    def reducer_count_acc(self, key, values):
        yield key, values

if __name__ == '__main__':
    CountCommonFollowers.run()
