import instaloader
import unicodecsv as csv
import time

L = instaloader.Instaloader()

# Login or load session
L.login("yasinhamidsk", "to1wcermwaps")

# Obtain profile metadata
SCRAPING_PROFILE = "richard_sulik"

profile = instaloader.Profile.from_username(L.context, SCRAPING_PROFILE)

index=0

# open the file in the write mode
f = open(SCRAPING_PROFILE+'.csv', 'wb')
# create the csv writer
writer = csv.writer(f, encoding="utf-8")
#writer = csv.writer(f, quoting=csv.QUOTE_ALL)
writer.writerow(['username','userid','full_name','biography','followers','followees','is_private'])

starTime = time.perf_counter()

# Print list of followers
for followee in profile.get_followers():
    username = followee.username

    row = []
    row.append(followee.username)
    row.append(followee.userid)
    row.append(followee.full_name)
    #bioUnaccented = unidecode.unidecode(bio)
    row.append(followee.biography)
    row.append(followee.followers) # how many followers he has
    row.append(followee.followees) # how many ppl he is following
    row.append(followee.is_private)
    writer.writerow(row)

    # write a row to the csv file
    elapsed_time = time.perf_counter() - starTime
    print("{}:{}:{}".format(index, elapsed_time, username))
    index = index + 1
    #if index % 10 == 0:
    #    break

f.close()




