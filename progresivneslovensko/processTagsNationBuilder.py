import psycopg2
from csv import reader

try:
    #local DB
    #connection = psycopg2.connect(user="postgres",
    #                              password="myTestPostgre",
    #                              host="127.0.0.1",
    #                              port="5432",
    #                              database="psTestDB")

    connection = psycopg2.connect(user="kisvmqrqkxnkct",
                                  password="c42b655ea26bc8ec88430c3d9b521824fac9966485dc2885c7b2b4537037d5c1",
                                  host="ec2-54-220-223-3.eu-west-1.compute.amazonaws.com",
                                  port="5432",
                                  database="d9ui8b306hhlgk")
    cursor = connection.cursor()

    tableName = 'CSV_TEMP'
    tableNameTags = 'CSV_TEMP_TAGS'

    cursor.execute("DROP TABLE IF EXISTS " + tableNameTags)

    # create tags table
    cursor.execute("""
                        CREATE TABLE CSV_TEMP_TAGS (
                            recId SERIAL PRIMARY KEY,
                            nationbuilder_id VARCHAR(255) NOT NULL,
                            tag VARCHAR(255) NOT NULL
                        )
                    """)

    cursor.execute("SELECT nationbuilder_id, tag_list FROM " + tableName)
    recordList = cursor.fetchall()
    cursor.close()

    cursor = connection.cursor()

    for record in recordList:
        nationBuilderId = record[0]
        tagsTxt = record[1]
        print(nationBuilderId + " : " +tagsTxt)
        tagsList = tagsTxt.split(",")

        for tag in tagsList:
            tagStripped = tag.replace("'","")
            tagStripped = tagStripped.strip()
            print(tagStripped)
            if "" == tagStripped:
                continue
            insertQuert = "INSERT INTO " + tableNameTags + " (nationbuilder_id, tag) VALUES (" + nationBuilderId + ", '" + tagStripped + "')"
            print(insertQuert)
            result = cursor.execute(insertQuert)
            print(result)
            print("+++++++++")



    connection.commit()


    #cursor.close()





except (Exception, psycopg2.Error) as error:
    print("Failed to insert record into mobile table", error)

finally:
    # closing database connection.
    if connection:
        cursor.close()
        connection.close()
        print("PostgreSQL connection is closed")


#Notes
# select id + tag select nationbuilder_id, trim(unnest(string_to_array(tag_list, ','))) from csv_temp;
#view name id_and_tags_list