import psycopg2
from csv import reader

try:
    #local DB
    #connection = psycopg2.connect(user="postgres",
    #                              password="myTestPostgre",
    #                              host="127.0.0.1",
    #                              port="5432",
    #                              database="psTestDB")

    connection = psycopg2.connect(user="kisvmqrqkxnkct",
                                  password="c42b655ea26bc8ec88430c3d9b521824fac9966485dc2885c7b2b4537037d5c1",
                                  host="ec2-54-220-223-3.eu-west-1.compute.amazonaws.com",
                                  port="5432",
                                  database="d9ui8b306hhlgk")
    cursor = connection.cursor()

    tableName = 'CSV_TEMP'
    tableNameTags = 'CSV_TEMP_TAGS'

    # open file in read mode
    with open('resources/pstagtest.csv', 'r', encoding='utf-8') as read_obj:
        # pass the file object to reader() to get the reader object
        csv_reader = reader(read_obj)
        # Iterate over each row in the csv using reader object
        firstRowRead = False
        header = []
        tagListColumnIndex = 0
        for row in csv_reader:
            if firstRowRead == False:
                header = row.copy()
                firstRowRead = True
                #drop table
                cursor.execute("DROP TABLE IF EXISTS " + tableName)



                columnIndex = 0
                crtTable = "CREATE TABLE " + tableName + "("
                for column in header:
                    crtTable = crtTable + column + " VARCHAR(255),"
                    if column == "tag_list":
                        tagListColumnIndex = columnIndex
                    columnIndex = columnIndex + 1

                crtTable = crtTable + " recId SERIAL PRIMARY KEY)"
                cursor.execute(crtTable)

            else:
                tblInsert = "INSERT INTO " + tableName+ " ("
                for column in header:
                    tblInsert = tblInsert + column + ","
                tblInsert = tblInsert[:-1]

                tblInsert = tblInsert + ") VALUES ("
                personId = -1
                columnIndex = 0

                for colValue in row:
                    colValueStriped = colValue.replace("'","")
                    tblInsert = tblInsert + "'" + colValueStriped +"',"
                    #if columnIndex == tagListColumnIndex:
                        #split tag list and create records in tag list table
                    columnIndex = columnIndex + 1

                tblInsert = tblInsert[:-1]

                tblInsert = tblInsert + ")"
                print(tblInsert)

                cursor.execute(tblInsert)
            #end else
        #end for

                    # row variable is a list that represents a row in csv
            #print(row)



    #postgres_insert_query = """ INSERT INTO mobile (ID, MODEL, PRICE) VALUES (%s,%s,%s)"""
    #record_to_insert = (5, 'One Plus 6', 950)
    #cursor.execute(postgres_insert_query, record_to_insert)

    connection.commit()
    count = cursor.rowcount
    print(count, "Record inserted successfully into mobile table")

except (Exception, psycopg2.Error) as error:
    print("Failed to insert record into mobile table", error)

finally:
    # closing database connection.
    if connection:
        cursor.close()
        connection.close()
        print("PostgreSQL connection is closed")


#Notes
# select id + tag select nationbuilder_id, trim(unnest(string_to_array(tag_list, ','))) from csv_temp;
#view name id_and_tags_list