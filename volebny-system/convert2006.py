import codecs


inputFile = open('./inputdata/2006 - Format A.csv', mode='r', encoding='utf8')
inputLines = inputFile.readlines()
inputFile.close()

outputFile = open('./inputdata/2006.csv', mode='w', encoding='utf8')

for line in inputLines:
    if line.startswith('Spolu za SR'):
        continue
    if line.startswith(','):
        continue
    outputFile.write(line)
