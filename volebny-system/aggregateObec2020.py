from mrjob.job import MRJob
from mrjob.step import MRStep
from mrjob.protocol import TextValueProtocol

# OBEC <> OKRES
#OBEC|OKRSOK|PS|P_HL_PS|P_HL_PS_PCT|P_VPH_PS|P_VPH_PS_PCT
#500011|1|1|1|0,17|1|100,00

class MRGroupResults2020(MRJob):

    INPUT_PROTOCOL = TextValueProtocol
    OUTPUT_PROTOCOL = TextValueProtocol


    def steps(self):
        return [
            MRStep(mapper=self.mapper_votes_per_party,
                   mapper_init=self.load_okresy,
                   reducer = self.reducer_aggregate_by_okres_and_party)
        ]

    def mapper_votes_per_party(self, key, line):
        lineElemets = line.split('|')
        obecId = int(lineElemets[0])
        okresName = self.obecOkresMap[obecId]
        yield okresName, int(lineElemets[3])

    def reducer_aggregate_by_okres_and_party(self, obecSubject, votes):
        yield obecSubject, sum(votes)


    def load_okresy(self):
        self.obecOkresMap = {}

        with open('C:/yha/python-projects/volebny-system/inputdata/Uzemne_clenenie2020.csv', encoding='utf8', mode='r') as ucFile:
            for line in ucFile:
                #KRAJ|NKRAJ|OBVOD|NOBVOD|OKRES|NOKRES|OBEC|NOBEC|TYP_OBCE|P_ALL_OKRSOK
                fields = line.split('|')
                okresName = fields[5]
                obecId = int(fields[6])
                self.obecOkresMap[obecId] = okresName


if __name__ == '__main__':
    MRGroupResults2020.run()