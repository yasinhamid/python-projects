import csv
from collections import OrderedDict

header = []

polSubjectsTmp = {}

with open('./inputdata/2020 PolitickeSubjekty.csv', mode="r", encoding='utf8') as politicalPartiesFile:
    for line in politicalPartiesFile:
        r = line.split('|')
        polSubjID = int(r[0])
        polSubjName = r[1]
        polSubjectsTmp[polSubjID] = polSubjName

polSubjects = OrderedDict(sorted(polSubjectsTmp.items()))

header.append("Názov okresu")
header.append("Počet platných hlasov spolu")
for key, ps in polSubjects.items():
    header.append(ps)

print(header)

totalVotes = {}

with open('./inputdata/aggregateOkres2020.txt', mode="r", encoding='utf8') as totalVotesFile:
    for line in totalVotesFile:
        lineSplit = line.split('\t');
        obecName = str(lineSplit[0]).replace('"','')
        votes = int(lineSplit[1])
        totalVotes[obecName] = votes

print("TOTAL VOTES")
print(totalVotes)

aggregateOkresPSTmp = {}

with open('./inputdata/aggregateOkresPS2020.txt', mode="r", encoding='utf8') as resultsFile:
    for line in resultsFile:
        lineSplit = line.split('\t');
        okresPolSubjId = lineSplit[0].zfill(2)
        numberOfVotes = int(lineSplit[1])
        aggregateOkresPSTmp[okresPolSubjId] = numberOfVotes


aggregateOkresPS = OrderedDict(sorted(aggregateOkresPSTmp.items()))

print("aggregateOkresPS")
#print(aggregateOkresPS)


print("resultMap")
resultMap = {}

for key, value in aggregateOkresPS.items():
    index = key.rfind("-")
    print(key)
    obecName = key[0:index].replace('"','')
    print(obecName)
    polSubjID = key[index + 1: len(key)].replace('"','')
    print(polSubjID)
    resultMap[obecName] = []

for key, value in aggregateOkresPS.items():
    index = key.rfind("-")
    obecName = key[0:index].replace('"','')
    polSubjID = key[index + 1: len(key)].replace('"','')
    resultMap[obecName].append(value)


with open('./inputdata/sumary2020.csv', mode="w", encoding='utf8', newline='') as outputFile:
    wtr = csv.writer(outputFile)
    wtr.writerow(header)
    for key, value in resultMap.items():
        r = []
        r.append(key)
        r.append(totalVotes[key])
        for v in value:
            r.append(v)
        wtr.writerow(r)


