import csv


with open('./inputdata/2010 - Format B.csv', mode="r", encoding='utf8') as source:
    rdr = csv.reader(source)
    with open('./inputdata/2010.csv', mode="w", encoding='utf8', newline='') as result:
        wtr = csv.writer(result)
        for r in rdr:
            del r[6]
            del r[5]
            del r[4]
            del r[3]
            del r[2]
            del r[0]
            for index, item in enumerate(r):
                r[index] = item.replace(',', '')
            wtr.writerow(r)
