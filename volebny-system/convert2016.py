import csv

#Read pol. parties

polParties = []

with open('./inputdata/2016 Zoznam PS  - Format C.csv', mode='r', encoding='utf8') as polPartiesInput:
    inputLines = polPartiesInput.readlines()
    for line in inputLines:
        polParties.append(line.split(",")[1])


with open('./inputdata/2016 - Format C.csv', mode="r", encoding='utf8') as source:
    rdr = csv.reader(source)
    with open('./inputdata/2016.csv', mode="w", encoding='utf8', newline='') as result:
        wtr = csv.writer(result)
        header = []
        header.append("Názov okresu")
        header.append("Počet platných hlasov spolu")
        for p in polParties:
            header.append(p)
        wtr.writerow(header)
        for r in rdr:
            rOut = []
            rOut.append(r[1])
            rOut.append(r[2])
            for index, item in enumerate(r):
                if (index -7 ) % 4 == 0:
                    rOut.append(r[index])
            for index, item in enumerate(rOut):
                rOut[index] = item.replace(',', '')
            wtr.writerow(rOut)
