import csv
from collections import OrderedDict

#Názov okresu,Počet platných hlasov spolu,EDS,Únia,SRK,Paliho Kapurková,SaS,SDĽ,SMK - MKP,ĽS - HZDS,KSS,SNS,ND,ZRS,KDH,ĽSNS,SDKÚ - DS,AZEN,SMER,MOST - HÍD
cutOffLimit = 0.05

fileNamePrefix = '2020'

inputFileName = './inputdata/' + fileNamePrefix + '.csv'
outputFileName = './inputdata/' + fileNamePrefix + 'result.txt'

print("******************************************************")
print("****************** ", fileNamePrefix, " ******************************")
print("******************************************************")

partyVotes = {}

okresWinners = {}

with open(inputFileName, mode="r", encoding='utf8') as inputFile:
    csvRrdr = csv.reader(inputFile)
    headerRead = False
    header = []
    for r in csvRrdr:
        if headerRead == False:
           header = r
           headerRead = True
           #print(header)
           continue
        okresName = r[0]
        #print(okresName)
        totalVotes = r[1]
        tempOkresWinners = {}
        for i in range(2, len(r)):
            polSubjectName = header[i]
            votes = int(r[i].replace(' ',''))
            #print(polSubjectName, " : ", votes)
            tempOkresWinners[votes] = polSubjectName
            totalPartyVotes = partyVotes.get(polSubjectName,0)
            totalPartyVotes = totalPartyVotes + votes
            partyVotes[polSubjectName] = totalPartyVotes
        sortedTempWiners = OrderedDict(sorted(tempOkresWinners.items(), reverse=True))
        votes, polSubject = next(iter(sortedTempWiners.items()))
        print("FIRST ITEM:", okresName, votes, polSubject)
        seats = okresWinners.get(polSubject, 0)
        #seats = okresWinners[polSubject]
        seats = seats + 1
        okresWinners[polSubject] = seats

print("Okres winners", okresWinners)

print("SEATS BY Okres")
totalSeatsByOkres = 0
for k, v in okresWinners.items():
    totalSeatsByOkres = totalSeatsByOkres + v
    print (k,v)

print("Total seats:" , totalSeatsByOkres)

print("TOTAL VOTES")
grandTotalVotes = 0
for polSubjName,votes in partyVotes.items():
    grandTotalVotes = grandTotalVotes + int(votes)
    #print(polSubjName, votes)

#caluclate number of seats
print("Grand total votes: ", grandTotalVotes)
cutOffNumberOfVotes = grandTotalVotes * cutOffLimit
print("Cut off votes: ", cutOffNumberOfVotes)

polSubjecstPassCutOff = {}
passedVotes = 0
for polSubjName,votes in partyVotes.items():
    if (polSubjName == "Koalícia Progresívne Slovensko a SPOLU - občianska demokracia"):
        print("!!!!!!!!!!!PS!!!!!!!!!!!!!!")
        continue
    if int(votes) > cutOffNumberOfVotes:
        polSubjecstPassCutOff[polSubjName] = votes
        passedVotes = passedVotes + votes


seatCoefficient = totalSeatsByOkres / passedVotes

seatsByPopularVote = {}

for polSubjName,votes in polSubjecstPassCutOff.items():
    seats = round(votes * seatCoefficient, 0)
    seatsByPopularVote[polSubjName] = int(seats)

print("seatsByPopularVote: ", seatsByPopularVote)

#combine
totalSeatsByParty = {}
for polSubjName,seats in okresWinners.items():
    totalSeats = totalSeatsByParty.get(polSubjName, 0)
    totalSeats = totalSeats + seats
    totalSeatsByParty[polSubjName] = totalSeats

for polSubjName,seats in seatsByPopularVote.items():
    totalSeats = totalSeatsByParty.get(polSubjName, 0)
    totalSeats = totalSeats + seats
    totalSeatsByParty[polSubjName] = totalSeats

ts = 0;
for polSubjName,seats in totalSeatsByParty.items():
    ts = ts + seats

print("Election year: ", fileNamePrefix)
print("Cut off limit: ", round(cutOffLimit*100,0), "%")
print("Parliament with total seats: ", ts)
print("Majority: ", round(ts/2,0) + 1)
print("Constitutional majority:", round(2*ts/3,0))
for polSubjName,seats in totalSeatsByParty.items():
    print(polSubjName, " ", seats)