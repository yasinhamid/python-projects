from mrjob.job import MRJob

class MRYhaCustomerSpending(MRJob):

    def mapper(self, _, line):
        (customerId, orderId, amountSpent) = line.split(',')
        yield customerId, float(amountSpent)

    def reducer(self, customerId, amounts):
        yield customerId, sum(amounts)


if __name__ == '__main__':
    MRYhaCustomerSpending.run()
