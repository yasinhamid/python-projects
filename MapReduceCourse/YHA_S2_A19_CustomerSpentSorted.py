from mrjob.job import MRJob
from mrjob.step import MRStep

class MRYhaCustomerSpendingSort(MRJob):
    
    def steps(self):
        return [
            MRStep(mapper=self.mapper_get_customer_amount,
                   reducer=self.reducer_sum_customer_amount),
            MRStep(mapper=self.mapper_switch_amount_customerId,
                   reducer = self.reducer_output_customer_Amount)
        ]
    

    def mapper_get_customer_amount(self, _, line):
        (customerId, orderId, amountSpent) = line.split(',')
        yield customerId, float(amountSpent)

    def reducer_sum_customer_amount(self, customerId, amounts):
        yield customerId, sum(amounts)
        
        
    def mapper_switch_amount_customerId(self, customerId, totalAmount):
        yield '%010.4f' % totalAmount, customerId

    def reducer_output_customer_Amount(self, totalAmount, customerIds):
        for customerId in customerIds:
            yield customerId, '%.2f' % float(totalAmount)


if __name__ == '__main__':
    MRYhaCustomerSpendingSort.run()
