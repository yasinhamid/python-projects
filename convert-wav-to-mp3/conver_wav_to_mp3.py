import os
from pydub import AudioSegment

def convert_wav_to_mp3(directory, output_directory):
    for root, dirs, files in os.walk(directory):
        for file in files:
            
            if file.lower().endswith(".wav"):
                wav_path = os.path.join(root, file)
                mp3_path = os.path.splitext(wav_path)[0] + ".mp3"
                mp3_path = mp3_path.replace(directory, output_directory)
                print(f"Converting {wav_path} to {mp3_path} ...")
                try:
                    audio = AudioSegment.from_wav(wav_path)
                    # Check if the directory exists
                    if not os.path.exists(os.path.dirname(mp3_path)):
                        # If not, create it
                        os.makedirs(os.path.dirname(mp3_path))
                    audio.export(mp3_path, format="mp3")
                    os.remove(wav_path)
                except Exception as e:
                    print(f"Failed to convert {wav_path} to {mp3_path}: {e}")
                    continue
                print(f"Converted {wav_path} to {mp3_path}")

# Usage example
convert_wav_to_mp3("c:\\temp\\drive-rozvod", "c:\\temp_out")